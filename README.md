# Fibonacci

A simple tool allowing a user to get a Fibonacci number from a given sequence number.

For example, given the sequence number 8, the Fibonacci number 21 will be returned.

## Set up

Set-up is a two-step process:

1. Install dependencies by running `npm install`.
2. Compile the TypeScript into JavaScript by running `npm run build`.

## Running the program

The Fibonacci tool can be run from the command line.

usage: `npm run start -- sequence_number`

### Example

```
> npm run start -- 9
34
```

## Tests

Tests are written in the Jest framework. To run tests do `npm run test`.
