import { getFibonacciNumber } from "../src/fibonacci";
describe("getFibonacciNumber", () => {
  test("fibonacci sequence", () => {
    const sequence = [
      0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597,
      2584, 4181, 6765,
    ];
    for (let i = 0; i < sequence.length; i++) {
      expect(getFibonacciNumber(i)).toBe(sequence[i]);
    }
  });

  test("out-of-range arguments throw an error", () => {
    expect(() => getFibonacciNumber(-1)).toThrow(RangeError);
    expect(() => getFibonacciNumber(-999)).toThrow(RangeError);
  });

  test("floating point arguments throw an error", () => {
    expect(() => getFibonacciNumber(50.5)).toThrow(TypeError);
    expect(() => getFibonacciNumber(0.1)).toThrow(TypeError);
  });

  test("other weird arguments throw errors", () => {
    expect(() => getFibonacciNumber(Number.EPSILON)).toThrow(TypeError);
    expect(() => getFibonacciNumber(Number.POSITIVE_INFINITY)).toThrow(
      TypeError
    );
    expect(() => getFibonacciNumber(Number.NEGATIVE_INFINITY)).toThrow(
      TypeError
    );
    expect(() => getFibonacciNumber(Number.NaN)).toThrow(TypeError);
  });

  test("length of time to do large numbers is less than a second.", () => {
    const start = new Date().getTime();
    getFibonacciNumber(35);
    const end = new Date().getTime();
    expect(end - start).toBeLessThan(1000);
  });
});
