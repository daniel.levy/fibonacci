export function getFibonacciNumber(n: number): number {
  if (!Number.isInteger(n)) {
    throw new TypeError("Argument 'n' must be an integer value.");
  }

  if (n < 0) {
    throw new RangeError("Argument 'n' must greater than or equal to 0.");
  }

  if (n <= 1) {
    return n;
  }

  let n1 = 0,
    n2 = 1,
    nOut = 0;

  for (let i = 2; i <= n; i++) {
    nOut = n1 + n2;
    n1 = n2;
    n2 = nOut;
  }

  return nOut;
}
