import { getFibonacciNumber } from "./fibonacci";

const args = process.argv.slice(2);

if (args.length == 0) {
  console.error("Please supply an argument.");
  process.exit();
}

const n = Number.parseInt(args[0]);

try {
  console.log(getFibonacciNumber(n));
} catch (error) {
  const errorMessage =
    error instanceof Error ? error.message : "Unknown error.";

  console.error(`Unable to run with arg value '${args[0]}': `, errorMessage);
}
